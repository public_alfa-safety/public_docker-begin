# Pré-requis

Installer nodeJS 10.x, docker et docker-compose :
* https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions
* https://docs.docker.com/install/linux/docker-ce/ubuntu/
* https://docs.docker.com/compose/install/#install-compose
* https://hub.docker.com/editions/community/docker-ce-desktop-windows?tab=description
* https://github.com/docker/toolbox/releases

Pour commencer, changez de branche pour progresser d'une étape à l'autre.
Pour chaque étape, la solution est présentée à l'étape suivante.
